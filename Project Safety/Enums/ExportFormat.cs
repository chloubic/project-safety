﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Enums
{
    public enum ExportFormat
    {

        BYTES = 0,
        HEX = 1,
        PLAIN_TEXT = 2

    }
}
