﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Enums
{
    public enum Initializer
    {

        PLAIN_TEXT = 0,
        HEX = 1,
        BYTES = 2

    }
}
