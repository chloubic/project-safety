﻿using AES_with_ECB___Console_App.Classes;
using AES_with_ECB___Console_App.Enums;
using AES_with_ECB___Console_App.Interfaces;
using System;
using System.IO;

namespace AES_with_ECB___Console_App
{
    class Program
    {
        static void Main(string[] args)
        {
            //This is just a test for my AES implementation.
            //now we try a key
            Text text;
            Key key;

            var menu = 0;
            bool inConsoleMode = false;
            bool outConsoleMode = false;
            bool cypherEncryptMode = false;
            string input;

            ICypher cypher = new AES(Mode.ECB);
            while(true)
            {
                Console.Clear();
                Console.WriteLine("KIV/BIT - Project Safety");
                Console.WriteLine("Author: Dominik Chlouba");
                Console.WriteLine();
                Console.WriteLine("------------------------------");
                Console.WriteLine();
                if (menu == 0)
                {
                    Console.WriteLine("Select input and output mode:");
                    Console.WriteLine("---------------");
                    Console.WriteLine("1) IN - Console, OUT - Console");
                    Console.WriteLine("2) IN - Console, OUT - File");
                    Console.WriteLine("3) IN - File, OUT - Console");
                    Console.WriteLine("4) IN - File, OUT - File");
                    Console.WriteLine("5) Exit");
                    input = GetInput();
                    if (input.Equals("1"))
                    {
                        inConsoleMode = true;
                        outConsoleMode = true;
                        menu = 1;
                    }
                    else if (input.Equals("2"))
                    {
                        inConsoleMode = true;
                        outConsoleMode = false;
                        menu = 1;
                    }
                    else if (input.Equals("3"))
                    {
                        inConsoleMode = false;
                        outConsoleMode = true;
                        menu = 1;
                    }
                    else if (input.Equals("4"))
                    {
                        inConsoleMode = false;
                        outConsoleMode = false;
                        menu = 1;
                    }
                    else if (input.Equals("5"))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Select one of the options...");
                        Console.WriteLine();
                    }
                }
                else if (menu == 1)
                {
                    WriteMode(inConsoleMode, outConsoleMode);
                    Console.WriteLine("------------------------------");
                    Console.WriteLine();
                    Console.WriteLine("Select cypher mode:");
                    Console.WriteLine("---------------");
                    Console.WriteLine("1) Encrypt");
                    Console.WriteLine("2) Decrypt");
                    Console.WriteLine("3) Back");
                    Console.WriteLine("4) Exit");
                    input = GetInput();
                    if (input.Equals("1"))
                    {
                        cypherEncryptMode = true;
                        menu = 2;
                    }
                    else if (input.Equals("2"))
                    {
                        cypherEncryptMode = false;
                        menu = 2;
                    }
                    else if (input.Equals("3"))
                    {
                        menu = 0;
                    }
                    else if (input.Equals("4"))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Select one of the options...");
                        Console.WriteLine();
                    }
                }
                else if (menu == 2)
                {
                    WriteMode(inConsoleMode, outConsoleMode);
                    Console.WriteLine("------------------------------");
                    Console.WriteLine();

                    Console.Write("Cypher Mode: ");
                    if (cypherEncryptMode) Console.Write("Encrypt\n");
                    else Console.Write("Decrypt\n");
                    Console.WriteLine();
                    Console.WriteLine("------------------------------");
                    Console.WriteLine();
                    if(inConsoleMode)
                    {
                        if (cypherEncryptMode) Console.WriteLine("Insert data (plain text):");
                        else Console.WriteLine("Insert data (HEX string):");
                        input = GetInput();
                        if (cypherEncryptMode)
                        {
                            text = new Text(input, Initializer.PLAIN_TEXT);
                        }
                        else
                        {
                            text = new Text(input, Initializer.HEX);
                        }
                        Console.WriteLine();
                        Console.WriteLine("Insert key (plain text):");
                        input = GetInput();
                        key = new Key(input, Initializer.PLAIN_TEXT);
                        bool back = Process(cypher, text, key, outConsoleMode, cypherEncryptMode);
                        if (back) menu = 1;
                    }
                    else
                    {
                        if (cypherEncryptMode) Console.WriteLine("Insert data file path (as plain text):");
                        else Console.WriteLine("Insert data file path (as HEX string):");
                        input = GetInput();
                        if (cypherEncryptMode)
                        {
                            text = new Text(File.ReadAllBytes(input), Initializer.BYTES);
                        }
                        else
                        {
                            text = new Text(File.ReadAllText(input), Initializer.HEX);
                        }
                        Console.WriteLine();
                        Console.WriteLine("Insert key (plain text):");
                        input = GetInput();
                        key = new Key(input, Initializer.PLAIN_TEXT);
                        bool back = Process(cypher, text, key, outConsoleMode, cypherEncryptMode);
                        if (back) menu = 1;
                    }
                }
            }
        }

        private static bool Process(ICypher cypher, Text text, Key key, bool outConsoleMode, bool cypherEncryptMode)
        {
            if (cypherEncryptMode)
            {
                var result = cypher.Encrypt(text, key);
                if(outConsoleMode)
                {
                    if (cypherEncryptMode)
                    {
                        Console.WriteLine("------------------------------");
                        Console.WriteLine();
                        Console.WriteLine("Data: " + text.PlainText);
                        Console.WriteLine("Key: " + key.PlainText);
                        Console.WriteLine();
                        Console.WriteLine("Result: " + result.GetHexString());
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("------------------------------");
                        Console.WriteLine();
                        Console.WriteLine("Data: " + text.Hex);
                        Console.WriteLine("Key: " + key.PlainText);
                        Console.WriteLine();
                        Console.WriteLine("Result: " + result.GetPlainTextString());
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Insert export data file path: ");
                    string exportPath = GetInput();
                    if (cypherEncryptMode) result.ExportAsFile(exportPath, ExportFormat.HEX);
                    else result.ExportAsFile(exportPath, ExportFormat.PLAIN_TEXT);
                }
            }
            else
            {
                var result = cypher.Decrypt(text, key);
                if (outConsoleMode)
                {
                    if (cypherEncryptMode)
                    {
                        Console.WriteLine("------------------------------");
                        Console.WriteLine();
                        Console.WriteLine("Data: " + text.PlainText);
                        Console.WriteLine("Key: " + key.PlainText);
                        Console.WriteLine();
                        Console.WriteLine("Result: " + result.GetHexString());
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("------------------------------");
                        Console.WriteLine();
                        Console.WriteLine("Data: " + text.Hex);
                        Console.WriteLine("Key: " + key.PlainText);
                        Console.WriteLine();
                        Console.WriteLine("Result: " + result.GetPlainTextString());
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Insert export data file path: ");
                    string exportPath = GetInput();
                    if (cypherEncryptMode) result.ExportAsFile(exportPath, ExportFormat.HEX);
                    else result.ExportAsFile(exportPath, ExportFormat.PLAIN_TEXT);
                }
            }
            Console.WriteLine("Press enter to continue or 1 to return into cypher mode menu...");
            var input = GetInput();
            if (input.Equals("1")) return true;
            else return false;
        }

        public static string GetInput()
        {
            Console.WriteLine();
            Console.Write("> ");
            return Console.ReadLine();
        }

        public static void WriteMode(bool inConsoleMode, bool outConsoleMode)
        {
            Console.Write("IN Mode: ");
            if (inConsoleMode) Console.Write("Console\n");
            else Console.Write("File\n");
            Console.Write("OUT Mode: ");
            if (outConsoleMode) Console.Write("Console\n");
            else Console.Write("File\n");
            Console.WriteLine();
        }
    }
}
