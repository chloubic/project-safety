﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Exceptions
{
    public class InvalidKeyLengthException : Exception
    {

        public InvalidKeyLengthException() : base("Key bigger than 128 bits (16 bytes)!") { }

    }
}
