﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Exceptions
{
    public class InvalidInitializerException : Exception
    {

        public InvalidInitializerException() : base("Used invalid initializer!") { }

    }
}
