﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Exceptions
{
    public class InvalidModeException : Exception
    {

        public InvalidModeException() : base("Used invalid AES mode!") { }

    }
}
