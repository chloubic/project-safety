﻿using AES_with_ECB___Console_App.Classes;
using AES_with_ECB___Console_App.DTOs;

namespace AES_with_ECB___Console_App.Interfaces
{
    public interface ICypher
    {

        ResultDto Encrypt(Text text, Key key);
        ResultDto Decrypt(Text text, Key key);

    }
}
