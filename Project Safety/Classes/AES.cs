﻿using AES_with_ECB___Console_App.DTOs;
using AES_with_ECB___Console_App.Enums;
using AES_with_ECB___Console_App.Exceptions;
using AES_with_ECB___Console_App.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AES_with_ECB___Console_App.Classes
{
    public class AES : ICypher
    {

        private Mode Mode { get; }

        public AES(Mode mode)
        {
            if (!Enum.IsDefined(typeof(Mode), mode)) throw new InvalidModeException();
            Mode = mode;
        }

        public ResultDto Decrypt(Text text, Key key)
        {
            if (Mode.Equals(Mode.ECB)) return ECBDecryptor.Decrypt(text, key);
            return null;
        }

        public ResultDto Encrypt(Text text, Key key)
        {
            if (Mode.Equals(Mode.ECB)) return ECBEncryptor.Encrypt(text, key);
            return null;
        }
    }
}
