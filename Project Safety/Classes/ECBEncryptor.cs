﻿using AES_with_ECB___Console_App.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class ECBEncryptor : ECBOperations
    {
        internal static ResultDto Encrypt(Text text, Key key)
        {
            var finalBytesList = new List<byte[]>();
            var keyClone = (Key)key.Clone();
            var repeat = text.Bytes.Length / Constants.BLOCK_SIZE;

            byte[] subtractArray;
            byte[] shiftArray;
            byte[] mixArray;
            Key generatedKey = null;

            for (var i = 1; i <= repeat; i++)
            {
                var xorArray = Xor(
                    ExtractValues(
                        text, 
                        (i - 1)*Constants.BLOCK_SIZE,
                        i*Constants.BLOCK_SIZE
                    ),
                    keyClone
                );
                for(var j = 1; j < 10; j++)
                {
                    generatedKey = (Key)keyClone.Clone();
                    for(var k = 0; k < j; k++)
                    {
                        generatedKey = GenerateKey(generatedKey, k);
                    }
                    subtractArray = SubtractBytes(xorArray);
                    shiftArray = ShiftRows(subtractArray);
                    mixArray = MixColumns(shiftArray);
                    xorArray = Xor(mixArray, generatedKey);
                }
                subtractArray = SubtractBytes(xorArray);
                shiftArray = ShiftRows(subtractArray);
                key = GenerateKey(generatedKey, 9);

                finalBytesList.Add(Xor(shiftArray, key));
            }

            return new ResultDto(finalBytesList);
        }

        internal static byte[] ShiftRows(byte[] substractArray)
        {
            var resultArray = new byte[substractArray.Length];
            Array.Copy(substractArray, resultArray, substractArray.Length);

            for(var i = 1; i < 4; i++)
            {
                for(var j = 0; j < i; j++)
                {
                    for(var k = 0; k < 3; k++)
                    {
                        var temp = resultArray[i];
                        resultArray[i] = resultArray[i + 12 - k * Constants.SEGMENT_SIZE];
                        resultArray[i + 12 - k * Constants.SEGMENT_SIZE] = temp;
                    }
                }
            }

            return resultArray;
        }

        internal static byte[] MixColumns(byte[] shiftArray)
        {
            var resultArray = new byte[shiftArray.Length];
            var counter = 0;

            for(var i = 0; i < Constants.SEGMENT_SIZE; i++)
            {
                for(var j = 0; j < Constants.SEGMENT_SIZE; j++)
                {
                    var cell = 0x00;
                    for(var k = 0; k < Constants.SEGMENT_SIZE; k++)
                    {
                        var current = shiftArray[k + Constants.SEGMENT_SIZE * i];
                        var old = current;
                        var matrixValue = Constants.MATRIX[k + j * Constants.SEGMENT_SIZE];
                        if (matrixValue == 2)
                        {
                            current = Multiply(current, old);
                        }
                        else if(matrixValue == 3)
                        {
                            current = Multiply(current, old, true);
                        }
                        cell = (byte)(cell ^ current);
                    }
                    resultArray[counter] = (byte)cell;
                    counter++;
                }
            }

            return resultArray;
        }

    }
}
