﻿using AES_with_ECB___Console_App.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class ECBDecryptor : ECBOperations
    {
        internal static ResultDto Decrypt(Text text, Key key)
        {
            var finalBytesList = new List<byte[]>();
            var keyClone = (Key)key.Clone();
            var repeat = text.Bytes.Length / Constants.BLOCK_SIZE;

            byte[] unSubtractArray;
            byte[] unShiftArray;
            byte[] unMixArray;
            Key generatedKey = null;

            for(var i = 0; i < 10; i++)
            {
                keyClone = GenerateKey(keyClone, i);
            }

            for(var i = 1; i <= repeat; i++)
            {
                var xorArray = Xor(
                    ExtractValues(
                        text,
                        (i - 1)*Constants.BLOCK_SIZE,
                        i*Constants.BLOCK_SIZE
                    ),
                    keyClone
                );

                unShiftArray = InvertShiftRows(xorArray);
                unSubtractArray = InvertSubtractBytes(unShiftArray);

                for(var j = 9; j > 0; j--)
                {
                    generatedKey = (Key)key.Clone();
                    for(var k = 0; k < j; k++)
                    {
                        generatedKey = GenerateKey(generatedKey, k);
                    }
                    xorArray = Xor(unSubtractArray, generatedKey);
                    unMixArray = InvertMixColumns(xorArray);
                    unShiftArray = InvertShiftRows(unMixArray);
                    unSubtractArray = InvertSubtractBytes(unShiftArray);
                }
                finalBytesList.Add(Xor(unSubtractArray, key));
            }

            return new ResultDto(finalBytesList);
        }

        internal static byte[] InvertShiftRows(byte[] array)
        {
            var resultArray = new byte[array.Length];
            Array.Copy(array, resultArray, array.Length);

            for (var i = 1; i < 4; i++)
            {
                for (var j = 0; j < i; j++)
                {
                    for (var k = 3; k > 0; k--)
                    {
                        var temp = resultArray[i + 12];
                        resultArray[i + 12] = resultArray[i + 12 - k * Constants.SEGMENT_SIZE];
                        resultArray[i + 12 - k * Constants.SEGMENT_SIZE] = temp;
                    }
                }
            }

            return resultArray;
        }

        internal static byte[] InvertSubtractBytes(byte[] array)
        {
            var resultArray = new byte[array.Length];

            for(var i = 0; i < array.Length; i++)
            {
                var higherValue = Convert.ToString((int)Math.Floor((double)Array.IndexOf(Constants.SBOX, array[i]) / Constants.BLOCK_SIZE), 16);
                var lowerValue = Convert.ToString((int)Math.Floor((double)Array.IndexOf(Constants.SBOX, array[i]) % Constants.BLOCK_SIZE), 16);
                resultArray[i] = HexStringToByteArrayConvertor.Process(higherValue + lowerValue)[0];
            }

            return resultArray;
        }

        internal static byte[] InvertMixColumns(byte[] array)
        {
            var resultArray = new byte[array.Length];
            var counter = 0;

            for (var i = 0; i < Constants.SEGMENT_SIZE; i++)
            {
                for (var j = 0; j < Constants.SEGMENT_SIZE; j++)
                {
                    var cell = 0x00;
                    for (var k = 0; k < Constants.SEGMENT_SIZE; k++)
                    {
                        var current = array[k + Constants.SEGMENT_SIZE * i];
                        var old = current;
                        var matrixValue = Constants.INVERSE_MATRIX[k + j * Constants.SEGMENT_SIZE];
                        if (matrixValue == 9)
                        {
                            for(var l = 0; l < 2; l++)
                            {
                                current = Multiply(current, old);
                            }
                            current = Multiply(current, old, true);
                        }
                        else if (matrixValue == 11)
                        {
                            current = Multiply(current, old);
                            for (var l = 0; l < 2; l++)
                            {
                                current = Multiply(current, old, true);
                            }
                        }
                        else if (matrixValue == 13)
                        {
                            current = Multiply(current, old, true);
                            current = Multiply(current, old);
                            current = Multiply(current, old, true);
                        }
                        else if (matrixValue == 14)
                        {
                            for (var l = 0; l < 2; l++)
                            {
                                current = Multiply(current, old, true);
                            }
                            current = Multiply(current, old);
                        }
                        cell = (byte)(cell ^ current);
                    }
                    resultArray[counter] = (byte)cell;
                    counter++;
                }
            }

            return resultArray;
        }
    }
}
