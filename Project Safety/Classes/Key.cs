﻿using AES_with_ECB___Console_App.Enums;
using AES_with_ECB___Console_App.Exceptions;
using System;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class Key : ICloneable
    {
        public string PlainText { get; set; }
        public string Hex { get; set; }
        public byte[] Bytes { get; set; }

        public Key(string key, Initializer initializer)
        {
            if (initializer.Equals(Initializer.PLAIN_TEXT)) InitFromPlainText(key);
            else if (initializer.Equals(Initializer.HEX)) InitFromHex(key);
            else throw new InvalidInitializerException();
            Check();
        }

        public Key(string plainText, string hex, byte[] bytes)
        {
            PlainText = plainText;
            Hex = hex;
            Bytes = bytes;
        }

        private void InitFromPlainText(string plainText)
        {
            PlainText = plainText;
            Bytes = Encoding.UTF8.GetBytes(PlainText);
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
        }

        public void InitFromBytes(byte[] bytes)
        {
            Bytes = bytes;
            PlainText = Encoding.UTF8.GetString(Bytes);
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
        }

        private void InitFromHex(string hex)
        {
            Hex = hex;
            Bytes = HexStringToByteArrayConvertor.Process(Hex);
            PlainText = Encoding.UTF8.GetString(Bytes);
        }

        private void Check()
        {
            if (Bytes.Length > Constants.BLOCK_SIZE) throw new InvalidKeyLengthException();
            else if (Bytes.Length < Constants.BLOCK_SIZE) Align();
        }

        private void Align()
        {
            var alignBytesArray = new byte[Constants.BLOCK_SIZE];
            Array.Copy(Bytes, alignBytesArray, Bytes.Length);
            Bytes = alignBytesArray;
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
        }

        public object Clone()
        {
            return new Key(PlainText, Hex, Bytes);
        }
    }
}
