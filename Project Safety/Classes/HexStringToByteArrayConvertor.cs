﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class HexStringToByteArrayConvertor
    {
        public static byte[] Process(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

    }
}
