﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class ECBOperations
    {

        internal static Key GenerateKey(Key key, int index)
        {
            var segmentArray = new byte[Constants.SEGMENT_SIZE];
            var generatedArray = new byte[Constants.BLOCK_SIZE];

            for (var i = 12; i < 16; i++)
            {
                segmentArray[i - 12] = key.Bytes[i];
            }
            for (var i = 3; i > 0; i--)
            {
                var temp = segmentArray[0];
                segmentArray[0] = segmentArray[i];
                segmentArray[i] = temp;
            }

            segmentArray = SubtractBytes(segmentArray);

            var counter = 0;
            while (counter < Constants.SEGMENT_SIZE)
            {
                generatedArray[counter] = (byte)(key.Bytes[counter] ^ segmentArray[counter] ^ Constants.RCON[Constants.SEGMENT_SIZE * index + counter]);
                counter++;
            }

            for (var i = Constants.SEGMENT_SIZE; i < Constants.BLOCK_SIZE; i++)
            {
                generatedArray[i] = (byte)(key.Bytes[i] ^ generatedArray[i - Constants.SEGMENT_SIZE]);
            }

            key.InitFromBytes(generatedArray);

            return key;
        }

        internal static byte[] SubtractBytes(byte[] segmentArray)
        {
            var resultArray = new byte[segmentArray.Length];

            for (var i = 0; i < segmentArray.Length; i++)
            {
                resultArray[i] = Constants.SBOX[(int)segmentArray[i]];
            }

            return resultArray;
        }

        internal static byte[] Xor(byte[] bytes, Key key)
        {
            var result = new byte[Constants.BLOCK_SIZE];

            for (var i = 0; i < Constants.BLOCK_SIZE; i++)
            {
                result[i] = (byte)(bytes[i] ^ key.Bytes[i]);
            }

            return result;
        }

        internal static byte[] ExtractValues(Text text, int start, int end)
        {
            var bytes = new byte[end - start];

            for (var i = start; i < end; i++)
            {
                bytes[i - start] = text.Bytes[i];
            }

            return bytes;
        }

        internal static byte Multiply(byte current, byte old, bool triple = false)
        {
            var oldValue = current;
            var newValue = current << 1;

            if ((oldValue & 0x80) == 0x80)
            {
                newValue = (byte)(newValue ^ 0x1B);
            }

            if (triple)
            {
                newValue ^= old;
            }

            return (byte)newValue;
        }

    }
}
