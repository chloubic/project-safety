﻿using AES_with_ECB___Console_App.Enums;
using AES_with_ECB___Console_App.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AES_with_ECB___Console_App.Classes
{
    public class Text
    {

        public string PlainText { get; set; }
        public string Hex { get; set; }
        public byte[] Bytes { get; set; }

        public Text(string text, Initializer initializer)
        {
            if (initializer.Equals(Initializer.PLAIN_TEXT)) InitFromPlainText(text);
            else if (initializer.Equals(Initializer.HEX)) InitFromHex(text);
            else throw new InvalidInitializerException();
            Check();
        }

        public Text(byte[] bytes, Initializer initializer)
        {
            if (initializer.Equals(Initializer.BYTES)) InitFromBytes(bytes);
            else throw new InvalidInitializerException();
            Check();
        }

        private void InitFromBytes(byte[] bytes)
        {
            Bytes = bytes;
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
            PlainText = Encoding.UTF8.GetString(Bytes);
        }

        private void InitFromPlainText(string plainText)
        {
            PlainText = plainText;
            Bytes = Encoding.UTF8.GetBytes(PlainText);
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
        }

        private void InitFromHex(string hex)
        {
            Hex = hex;
            Bytes = HexStringToByteArrayConvertor.Process(Hex);
            PlainText = Encoding.UTF8.GetString(Bytes);
        }

        private void Check()
        {
            if (Bytes.Length % Constants.BLOCK_SIZE != 0) Align();
        }

        private void Align()
        {
            var arraySize = ((int)(Bytes.Length / Constants.BLOCK_SIZE) + 1) * Constants.BLOCK_SIZE;
            var alignBytesArray = new byte[arraySize];
            Array.Copy(Bytes, alignBytesArray, Bytes.Length);
            Bytes = alignBytesArray;
            Hex = BitConverter.ToString(Bytes).Replace("-", "");
        }

    }
}
