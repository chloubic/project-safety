﻿using AES_with_ECB___Console_App.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AES_with_ECB___Console_App.DTOs
{
    public class ResultDto
    {

        private byte[] Bytes { get; }

        public string GetHexString()
        {
            return BitConverter.ToString(Bytes).Replace("-", "");
        }

        public string GetPlainTextString()
        {
            return Encoding.UTF8.GetString(Bytes);
        }

        public void ExportAsFile (string path, ExportFormat format)
        {
            if (format.Equals(ExportFormat.PLAIN_TEXT))
            {
                File.WriteAllBytes(path, Bytes);
            }
            else File.WriteAllText(path, GetHexString());
            Console.WriteLine();
            Console.WriteLine($"File exported ({path})...");
            Console.WriteLine();
            return;
        }

        public ResultDto(List<byte[]> bytesList)
        {
            Bytes = new byte[bytesList.Count*Constants.BLOCK_SIZE];
            var index = 0;
            foreach(var byteArray in bytesList)
            {
                foreach(var byteValue in byteArray)
                {
                    Bytes[index] = byteValue;
                    index++;
                }
            }
        }

    }
}
